var fs = require('fs');
const productsData = JSON.parse(fs.readFileSync('products_data.json', 'utf8'));
const _ = require('loadsh');

// console.log("productsData/products ", productsData.products);

function apply2For1Discount({count, price}) {

  // divide into chunk of 3
  let chunkedData = _.chunk([...Array(count).keys()], 3);
  let total = 0;

  // console.log("apply2For1Discount/chunkedData ", chunkedData);
  // console.log("apply2For1Discount/count ", count);

  for (let chunk of chunkedData) {
    // console.log("apply2For1Discount/chunk.length ", chunk.length);
    // if chunk size is 1 then pay for 1 item, if 2 then pay for 1 and get 1 crossed out with the 3 to be received as offer
    if (chunk.length <= 2)
      total += price;
    else
      // if chunk size is 3 then pay for 2 items
      total +=  price * 2;
  }

  return total;
}

function applyBulkPurchaseDiscountDiscount({count, price, discountTypeData}) {
  if (count >= discountTypeData.minQuantity)
    return count * discountTypeData.discountedPrice;

  return count * price;
}

/**
 * calculate discounted total
 *
 * RESTRICTIONS:
 *  - here if discount type not supported then not throwing error, but just returning total without discount, ideally should throw error
 *  - Only one type of discount can be applied on a product. If multiple to be applied on the same product, then need to change the code.
 *    Assuming its out of scope and won't happen most likely
 *
 * @param count
 * @param price
 * @param discountData
 * @returns {number|*}
 */
function applyDiscount({count, price, discountData}) {

  // console.log("applyDiscount/discountData ", discountData);

  let discountType = _.keys(discountData)[0];
  let discountTypeData = discountData[discountType];

  // console.log("applyDiscount/discountType ", discountType);
  // console.log("applyDiscount/discountTypeData ", discountTypeData);

  switch (discountType) {
    case "2-for-1":
      return apply2For1Discount({count, price});
    case "bulk-purchase-discount":
      return applyBulkPurchaseDiscountDiscount({count, price, discountTypeData});
  }

  return count * price;
}

/**
 * Returns simple total if no discount, otherwise returned discounted Total
 * @param items
 * @returns {number}
 */
function calculateItemTotal({item, count}) {

  // console.log("calculateItemTotal/item ", item);
  // console.log("calculateItemTotal/count ", count);

  let itemData = productsData.products[item];

  // console.log("calculateItemTotal/itemData ", itemData);

  if (itemData.discount) {
    return applyDiscount({count, price: itemData.price, discountData: itemData.discount});
  }

  return count * itemData.price;
}

/**
 * Group items by type, calculate total and add them up
 * @param items
 * @returns {number}
 */
function calculateTotal({items}) {
  let groupedItems = _.groupBy(items);

  // console.log("calculateTotal/groupedItems ", groupedItems);

  let total = 0.0;

  for (let groupedItem of _.keys(groupedItems)) {
    // console.log("calculateTotal/groupedItem ", groupedItem);

    total += calculateItemTotal({item: groupedItem, count: groupedItems[groupedItem].length, items: groupedItems[groupedItem]});

    // console.log("calculateTotal/groupedItem/total ", total);
  }

  return total;
}

console.log("result : [\"VOUCHER\", \"TSHIRT\", \"MUG\"] : ", calculateTotal({items: ["VOUCHER", "TSHIRT", "MUG"]}));
console.log("result : [\"VOUCHER\", \"TSHIRT\", \"VOUCHER\"] : ", calculateTotal({items: ["VOUCHER", "TSHIRT", "VOUCHER"]}));
console.log("result : [\"TSHIRT\", \"TSHIRT\", \"TSHIRT\", \"VOUCHER\", \"TSHIRT\"] : ", calculateTotal({items: ["TSHIRT", "TSHIRT", "TSHIRT", "VOUCHER", "TSHIRT"]}));
console.log("result : [\"VOUCHER\", \"TSHIRT\", \"VOUCHER\", \"VOUCHER\", \"MUG\", \"TSHIRT\", \"TSHIRT\"] : ", calculateTotal({items: ["VOUCHER", "TSHIRT", "VOUCHER", "VOUCHER", "MUG", "TSHIRT", "TSHIRT"]}));
console.log("result : [\"VOUCHER\", \"TSHIRT\", \"VOUCHER\", \"VOUCHER\", \"MUG\", \"VOUCHER\", \"TSHIRT\", \"TSHIRT\"] : ", calculateTotal({items: ["VOUCHER", "TSHIRT", "VOUCHER", "VOUCHER", "MUG", "VOUCHER", "TSHIRT", "TSHIRT"]}));
console.log("result : [\"VOUCHER\", \"VOUCHER\", \"TSHIRT\", \"VOUCHER\", \"VOUCHER\", \"MUG\", \"VOUCHER\", \"TSHIRT\", \"TSHIRT\"] : ", calculateTotal({items: ["VOUCHER", "VOUCHER", "TSHIRT", "VOUCHER", "VOUCHER", "MUG", "VOUCHER", "TSHIRT", "TSHIRT"]}));
